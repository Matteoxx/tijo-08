package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PawnTest {

    private RulesOfGame pawn;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForPawn() {

        pawn = new Pawn();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForPawn() {

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(pawn.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(5, 6);
        assertTrue(pawn.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 2);
        pointTo = new Point2d(5, 4);
        assertTrue(pawn.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(9, 9);
        pointTo = new Point2d(9, 7);
        assertTrue(pawn.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(9, 9);
        pointTo = new Point2d(9, 8);
        assertTrue(pawn.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 2);
        pointTo = new Point2d(5, 3);
        assertTrue(pawn.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(6, 5);
        assertFalse(pawn.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(4, 5);
        assertFalse(pawn.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);
    }
}

package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KingTest {

    private RulesOfGame king;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForKing() {

        king = new King();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForKing() {

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(0, 0);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(0, 1);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(1, 1);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(2, 1);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(2, 0);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(2, -1);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(1, -1);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(0, -1);
        assertTrue(king.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, -2);
        pointTo = new Point2d(-1, 0);
        assertFalse(king.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(-1, -2);
        pointTo = new Point2d(-3, 0);
        assertFalse(king.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(king.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);
    }
}
